import 'dart:ui';
import 'package:flutter/material.dart';

class menuItems extends StatelessWidget {
  menuItems({@required this.image, @required this.title});

  String image;
  String title;

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Container(
              child: Image.asset('assets/images/${image}', scale: 1.5,),
              width: 70,
              height: 70,
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.all(
                      Radius.circular(25)
                  ),
                  color: const Color(0xfffbfbfb)
              )
          ),
          Text(
              title,
              style: const TextStyle(
                  color:  const Color(0xff929292),
                  fontFamily: "Assistant",
                  fontSize: 18.0,
                  fontWeight: FontWeight.w600,
              ),
              textAlign: TextAlign.center
          ),
        ]
    );
  }
}