import 'dart:ui';
import 'package:flutter/material.dart';
import 'package:manager_template/components/menu_items.dart';

const String customBarTitle = 'דוחות';
const String customBarIcon = '-e-icons8-book-shelf-100 copy.png';

class CustomBarWidget extends StatelessWidget {
  GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color(0xfff3f3f6),
      key: _scaffoldKey,
      ///custom bar
      appBar: PreferredSize(
        preferredSize: Size.fromHeight(190),
        child: Stack(
          fit: StackFit.expand,
          children: <Widget>[
            Container(
              height: 160.0,
              child: Stack(
                children: <Widget>[
                  Container(
                    width: MediaQuery.of(context).size.width,
                    height: 130.0,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.only(bottomRight:  Radius.circular(80), bottomLeft:  Radius.circular(80)),
                      color: Color(0xff46ae9f),
                    ),
                    child: Center(
                      child: Padding(
                          padding: const EdgeInsets.only(bottom: 40.0),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Text(
                                  'דוחות',
                                  style: const TextStyle(
                                      color:  Colors.white,
                                      fontFamily: "Assistant",
                                      fontSize: 25,
                                      fontWeight: FontWeight.w600,
                                  ),
                                  textAlign: TextAlign.center
                              ),
                              SizedBox(width: 10,),
                              Image.asset('assets/images/${customBarIcon}', scale: 1.8),
                            ],)
                      ),
                    ),
                  ),
                  Positioned(
                    top: 80.0,
                    left: 0.0,
                    right: 0.0,
      ///custom bar items container
                    child: Container(
                      height: 120,
                      padding: EdgeInsets.symmetric(horizontal: 100.0),
                      child: DecoratedBox(
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(18.0),
                            color: Colors.white),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          children: [
      ///custom bar items
                            GestureDetector(onTap:
                                (){
                              print('Sending');
                            },
                              child: menuItems(
                                title: 'שליחה לרו״ח',
                                image: 'icons8-ledger-100.png',
                              ),
                            ),
                            GestureDetector(onTap:
                                (){
                              print('Sending');
                            },
                              child: menuItems(
                                title: 'Z',
                                image: 'icons8-money-transfer-100 (1).png',
                              ),
                            ),
                            GestureDetector(onTap:
                                (){
                              print('Sending');
                            },
                              child: menuItems(
                                title: 'עסקאות',
                                image: 'icons8-handshake-100.png',
                              ),
                            ),
                            GestureDetector(onTap:
                                (){
                              print('Sending');
                            },
                              child: menuItems(
                                title: 'שעון נוכחות',
                                image: '-e-icons8-schedule-100 copy.png',
                              ),
                            ),
                            GestureDetector(onTap:
                                (){
                              print('Sending');
                            },
                              child: menuItems(
                                title: 'פריטים',
                                image: 'icons8-doughnut-chart-100.png',
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                  )
                ],
              ),
            ),
          ],),),
      ///screen body
    body: Center(child: Text('Text'),),
    );
  }
}
